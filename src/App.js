// import React from "react";
// import User from "./user";
// export default class App extends React.Component {
//   state = {
//     balance: 13000,
//     show:true
//   }
//   // componentWillMount(){
//   //   console.log("ok")
//   // }
//   // componentDidMount(){
//   //   console.log("hello");
//   // }
//   shouldComponentUpdate(){
//     return false;
//   }
//   componentWillUpdate(){
//     alert("ok");
//     return false;
//   }

//   changed = () => {
//     this.setState(
//       { balance: this.state.balance + 100 }
//     )

//   }
//   render() {
//     return (
//       <>
//         <div>
//           <User balance={this.state.balance}/>
//           <button onClick={this.changed}>change Balans</button>
//           {/* <button onClick={Show}>click</button> */}
//         </div>
//       </>
//     )
//   }
// }
import React from "react";
import User from "./user";
// export default class App extends React.Component{
//   state={
//     show:true,
//   }
//   shoow=()=>{
//     this.setState(state=>(state.show=!state.show))
//   }
//   render(){
//     return(
//       <>
//       <User functionShow={this.shoow} isShow={this.state.show}/>
//       <h1>Bu App Component</h1>
//       <button onClick={this.shoow}>{this.state.show ? "Hide":"Show"}</button>
//       </>
//     )
//   }
// }
export default class App extends React.Component{
  state={
    text:"My Component",
    show:true
  }
  componentDidMount() {
    console.log(" componentDidMount ishladi");
  }
  deleted=()=>{
    this.setState((state)=>(state.show=!state.show))
  }
  added=()=>{
    this.setState((state) => (state.show = !state.show))
  }
  render(){
    return(
      <div style={{
        display:"flex",
        justifyContent:"center",
      }}>
      <div style={{
        display:"flex",
        width:"500px",
        height:"500px",
        border:"2px solid black",
        alignItems:"center",
        justifyContent:"center",
        gap:"10px"
      }}>
        <button onClick={this.added}>show</button>
        <button onClick={this.deleted}>hide</button>
        <User text={this.state.text} isShow={this.state.show}/>
      </div>
      </div>
    )
  }
}